#! /usr/bin/bash

cat <<EOF
When prompted (assuming users have not been modified), 
please enter in the following passwords. 
SSH Password: alarm
root password: root

This must be entered for every change needing to be made. 

EOF

ansible nodes -m "raw" -a "pacman-key --init" -u alarm --become --become-method "su" -kK
ansible nodes -m "raw" -a "pacman-key --populate archlinuxarm" -u alarm --become --become-method "su" -kK
ansible nodes -m "raw" -a "pacman -Syy" -u alarm --become --become-method "su" -kK
ansible nodes -m "raw" -a "pacman -Su --noconfirm" -u alarm --become --become-method "su" -kK
ansible nodes -m "raw" -a "pacman -S python --noconfirm" -u alarm --become --become-method "su" -kK

unset ssh_pass
unset root_pass
