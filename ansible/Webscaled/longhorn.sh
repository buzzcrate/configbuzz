#!/bin/bash

LONGHORN_VERSION='1.4.0'
# Installing Longhorn to a cluster

# Enable iscsi as a prereq
ansible nodes -m systemd -a "name=iscsid state=started enabled=yes" --become

# Assume kube config has been downloaded already
kubectl apply -f https://raw.githubusercontent.com/longhorn/longhorn/v${LONGHORN_VERSION}/deploy/longhorn.yaml

