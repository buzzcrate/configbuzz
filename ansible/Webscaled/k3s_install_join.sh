#!/bin/bash
# Install k3s and join the nodes on the newly provisioned nodes. 

ansible-playbook k3s_install_join.yml --skip-tags "disjoin,uninstall"
