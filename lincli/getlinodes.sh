#!/bin/bash
source bin/activate

ANSIBLE_HOME=../ansible/Webscaled
# Get machines from Linode
linode-cli linodes list --json > listjson.json
# Create host file from inventory hosts
python invcreate.py listjson.json \
 $ANSIBLE_HOME/linodeinv.hosts \
 ../tf/variables.tf \
 $ANSIBLE_HOME/linodelogin.hosts

cat $ANSIBLE_HOME/linodeinv.hosts $ANSIBLE_HOME/base.hosts \
$ANSIBLE_HOME/linodelogin.hosts \
> $ANSIBLE_HOME/linode.hosts
